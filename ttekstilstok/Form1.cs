﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ttekstilstok
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            List<ALICI> Alıcı_No = new List<ALICI>();
            MERVEEntities db = new MERVEEntities();

            Alıcı_No = db .ALICI. ToList ();
            dataGridView1.DataSource = Alıcı_No;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            
            List<ALICI> Alıcı_Adı  = new List<ALICI>();
            MERVEEntities db = new MERVEEntities();

            Alıcı_Adı = db. ALICI .ToList ();
            dataGridView1.DataSource = Alıcı_Adı ;

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            
            List<ALICI> Alıcı_Soyadı = new List<ALICI>();
            MERVEEntities db = new MERVEEntities();

            Alıcı_Soyadı = db. ALICI .ToList ();
            dataGridView1.DataSource = Alıcı_Soyadı ;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MERVEEntities db =new  MERVEEntities();
            List<ALICI> Lİst = db.ALICI.ToList();


            dataGridView1.DataSource = Lİst ;




        }

        private void button2_Click(object sender, EventArgs e)
        {
          MERVEEntities db = new MERVEEntities() ;
          List<ALICI> Stok_No = db.ALICI.Take(2).ToList();

          dataGridView1.DataSource = Stok_No;


        }

        private void button3_Click(object sender, EventArgs e)
        {

            MERVEEntities db = new MERVEEntities();
            List<ALICI> ALICI_NO = db.ALICI.OrderBy(a=> a.ALICI_NO).ToList();
 

            dataGridView1.DataSource = ALICI_NO; 
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MERVEEntities db = new MERVEEntities();
            int enkucuk = db.ALICI.Min(a => a.ID);
            int enbuyuk = db.ALICI.Max(a => a.ID);
            int toplam = db.ALICI.Sum(a => a.ID);

            MessageBox.Show(enkucuk.ToString());
            MessageBox.Show(enbuyuk.ToString());
            MessageBox.Show(toplam.ToString());
        }

        private void Ürün_Click(object sender, EventArgs e)
        {
            MERVEEntities db = new MERVEEntities();

            List<ÜRETİCİ> liste = db.ÜRETİCİ.ToList();

            var linqliste = (from m in liste where m.ÜRÜN_NO > 4 select m).ToList();

            dataGridView1.DataSource = linqliste;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MERVEEntities db=new MERVEEntities();
            List<ALICI> liste = db.ALICI.ToList();


            var linqliste = (from b in liste where b.ALICI_NO >= 6 select new { No = b.ALICI_NO, Stk = b.STOK_NO }).ToList ();
            dataGridView1.DataSource = linqliste;

        }

        private void button6_Click(object sender, EventArgs e)
        { 
            MERVEEntities db =new MERVEEntities();
            List<ALICI>liste =db.ALICI.ToList();
            var linqtoplam = (from c in liste where c.STOK_NO >=85 select c ).Sum (c=>c.STOK_NO);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            MERVEEntities db = new MERVEEntities();
            int secilenID = int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
           ALICI m = db.ALICI.Where(a => a.ID == secilenID).First();

            Form2 f = new Form2(m);
            f.Show();
  

        }
            
            
    }
}
